import { Component, OnInit } from '@angular/core';
import { ArtInfoProviderService } from '../shared/art-info-provider.service';
import { PaintingModel } from '../shared/painting-model';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  paintings: PaintingModel[];
  selectedPainting: PaintingModel;

  constructor(private artInfoProvider: ArtInfoProviderService) {
    this.paintings = [...artInfoProvider.paintings];
  }

  ngOnInit(): void {
  }


  selectPainting(painting: PaintingModel) {
    this.selectedPainting = painting;
  }
}
