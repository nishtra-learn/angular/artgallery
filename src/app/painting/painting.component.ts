import { Component, Input, OnInit } from '@angular/core';
import { ArtInfoProviderService } from '../shared/art-info-provider.service';
import { PaintingModel } from '../shared/painting-model';

@Component({
  selector: 'app-painting',
  templateUrl: './painting.component.html',
  styleUrls: ['./painting.component.css']
})
export class PaintingComponent implements OnInit {
  @Input() painting: PaintingModel;

  constructor(private artInfoProvider: ArtInfoProviderService) { 
    //this.painting = artInfoProvider.paintings[0];
  }

  ngOnInit(): void {
  }

}
