import { Injectable } from '@angular/core';
import { PaintingModel } from './painting-model';

@Injectable({
  providedIn: 'root'
})
export class ArtInfoProviderService {
  paintings: PaintingModel[] = [];

  constructor() {
    this.paintings.push({
      href: '../../assets/images/Michelangelo_-_Creation_of_Adam.jpg',
      title: 'The Creation of Adam',
      author: 'Michelangelo',
      yearOfCreation: 1512,
      dimensions: {
        height: 280,
        width: 570
      },
      location: 'Sistine Chapel, Vatican'
    });

    this.paintings.push({
      href: '../../assets/images/Mona_Lisa,_by_Leonardo_da_Vinci.jpg',
      title: 'Mona Lisa',
      author: 'Leonardo DaVinci',
      yearOfCreation: 1502,
      dimensions: {
        height: 53,
        width: 76.8
      },
      location: 'Louvre, Paris'
    });

    this.paintings.push({
      href: '../../assets/images/the-persistence-of-memory-1931-1140x867.jpg',
      title: 'The Persistence of Memory',
      author: 'Salvador Dalí',
      yearOfCreation: 1931,
      dimensions: {
        height: 33,
        width: 24
      },
      location: 'Museum of Modern Art, New York City'
    });
   }
}
