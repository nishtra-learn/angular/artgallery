import { Dimensions } from './dimensions';

export class PaintingModel {
    href: string;
    title: string;
    yearOfCreation: number;
    author: string;
    dimensions: Dimensions;
    location: string;
}
